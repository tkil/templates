# Setup
These are the instructions to get eslint and prettier to coexist and
for vscode to give you active feedback on linting errors before save.
Step #1 should make it so that you can roll on to a eslint or prettier
project and follow the rules set for that repo.


## Step 1
Add the following to the root of your vs-code settings file (hotkey: cmd ,)

```json
  /**************************************************/
  /* ESLINT */
  /**************************************************/
  "[html]": { "editor.defaultFormatter": "dbaeumer.vscode-eslint" },
  "[javascript]": { "editor.defaultFormatter": "dbaeumer.vscode-eslint" },
  "[json]": { "editor.defaultFormatter": "dbaeumer.vscode-eslint" },
  "[typescript]": { "editor.defaultFormatter": "dbaeumer.vscode-eslint" },
  "[typescriptreact]": { "editor.defaultFormatter": "dbaeumer.vscode-eslint" },
  "eslint.validate": [ "javascript", "javascriptreact",
      { "language": "typescript", "autoFix": true },
      { "language": "typescriptreact", "autoFix": true }
  ],
  "eslint.alwaysShowStatus": true,
  "eslint.experimental.incrementalSync": true,
  "eslint.lintTask.enable": true,
  /**************************************************/
```

## Step 2
npm install the following to a project to apply the linting rules

```bash
npm install -D \
eslint \
eslint-config-prettier \
eslint-plugin-jest \
eslint-plugin-prettier \
prettier \
typescript \
@typescript-eslint/parser
```

## Step 3
We need to tell eslint that prettier is the source of truth for the rules.\
Create a `.eslintrc` file with the following...
```json
{
  "env": {
    "jest/globals": true
  },
  "extends": ["plugin:prettier/recommended"],
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "ecmaVersion": 6,
    "sourceType": "module"
  },
  "plugins": [ "prettier", "jest" ],
  "rules": {
    "prettier/prettier": "error"
  }
}
```

## Step 4
Add your `.prettierrc` with any overwrites (if needed)
```json
{
  "singleQuote": false,
  "semi": true,
  "printWidth": 120
}
```