const path = require('path')
const CopyPlugin = require('copy-webpack-plugin')

const npmPackages = [
  'copy-webpack-plugin'
]

const tsFiles = [
  './src/handler.ts'
]

module.exports = {
  target: 'node',
  mode: 'production',
  entry: {
    handler: './src/handler.ts',
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
    libraryTarget: 'commonjs'
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new CopyPlugin([
      { from: 'serverless.yml' }
    ]),
  ],
}
